#include "CsoundWwiseEffectFX.h"
#include "../CsoundWwiseEffectConfig.h"

#include <AK/AkWwiseSDKVersion.h>
#include "../../Common/Utility.hpp"

AK::IAkPlugin* CreateCsoundWwiseEffectFX(AK::IAkPluginMemAlloc* in_pAllocator)
{
    return AK_PLUGIN_NEW(in_pAllocator, CsoundWwiseEffectFX());
}

AK::IAkPluginParam* CreateCsoundWwiseEffectFXParams(AK::IAkPluginMemAlloc* in_pAllocator)
{
    return AK_PLUGIN_NEW(in_pAllocator, ParameterManager());
}

AK_IMPLEMENT_PLUGIN_FACTORY(CsoundWwiseEffectFX, AkPluginTypeEffect, CsoundWwiseEffectConfig::CompanyID, Utility::GetIDFromXML())

CsoundWwiseEffectFX::CsoundWwiseEffectFX()
    : m_pParams(nullptr)
    , m_pAllocator(nullptr)
    , m_pContext(nullptr)
    , m_csoundManager(nullptr) {}

CsoundWwiseEffectFX::~CsoundWwiseEffectFX(){}

AKRESULT CsoundWwiseEffectFX::Init(AK::IAkPluginMemAlloc* in_pAllocator, AK::IAkEffectPluginContext* in_pContext, AK::IAkPluginParam* in_pParams, AkAudioFormat& in_rFormat)
{
    m_pParams = (ParameterManager*)in_pParams;

    if (!m_pParams->IsCompiled())
        return AK_InvalidFile;

    m_pAllocator = in_pAllocator;
    m_pContext = in_pContext;
    m_csoundManager = m_pParams->GetCsoundManager();
    in_rFormat.channelConfig.SetStandard(m_csoundManager->GetChannelMask());

    return AK_Success;
}

AKRESULT CsoundWwiseEffectFX::Term(AK::IAkPluginMemAlloc* in_pAllocator)
{
    AK_PLUGIN_DELETE(in_pAllocator, this);
    return AK_Success;
}

AKRESULT CsoundWwiseEffectFX::Reset()
{
    return AK_Success;
}

AKRESULT CsoundWwiseEffectFX::GetPluginInfo(AkPluginInfo& out_rPluginInfo)
{
    out_rPluginInfo.eType = AkPluginTypeEffect;
    out_rPluginInfo.bIsInPlace = true;
    out_rPluginInfo.uBuildVersion = AK_WWISESDK_VERSION_COMBINED;
    return AK_Success;
}

void CsoundWwiseEffectFX::Execute(AkAudioBuffer* io_pBuffer)
{
    m_pParams->HandleParameterChange(m_csoundManager);
    m_csoundManager->Process(io_pBuffer, true);
}

AKRESULT CsoundWwiseEffectFX::TimeSkip(AkUInt32 in_uFrames)
{
    return AK_DataReady;
}
