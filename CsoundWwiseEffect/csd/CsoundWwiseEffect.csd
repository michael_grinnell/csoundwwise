<Cabbage>
form caption("SimpleEffectPlugin") size(128, 192), colour(127, 127, 127), pluginid("0") 

hslider bounds(0, 64, 128, 128)channel("Size")range(0, 1, .6, 1, 0.01)
rslider bounds(0, 0, 64, 64) channel("CutOFF")range(20, 20000, 8000, 1, 10)
rslider bounds(64, 0, 64, 64) channel("Gain")range(0, 1, .5, 1, 0.001)

</Cabbage> 
<CsoundSynthesizer>
<CsOptions>
-n -d 
</CsOptions>
<CsInstruments>
; Initialize the global variables. 
sr = 48000
ksmps = 32
nchnls = 2
0dbfs = 1

instr 1
    aInL inch 1
    aInR inch 2
    
    kSize chnget "Size"
    kCutOff chnget "CutOFF"
    kGain chnget "Gain"
    
    aOutL, aOutR reverbsc aInL, aInR, kSize, kCutOff
   
    outs aOutL*kGain, aOutR*kGain
endin

</CsInstruments>
<CsScore>
f1 0 1024 10 1
i1 0 z
</CsScore>
</CsoundSynthesizer>