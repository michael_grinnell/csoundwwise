# CsoundWwise
#### Current Version: 0.1

This Repo is for development of a project that aims the extend the Wwise audio middleware API to support the Csound audio programming language.


## How do I get set up?

### Prerequisites

* [Visual Studio 2019](https://visualstudio.microsoft.com/downloads/) with Windows SDK **10.0.17763.0** &  **10.0.14393.795**
* Wwise Audio Middleware and the Wwsie SDK, these can be installed from the [Wwise Launcher](https://www.audiokinetic.com/download/)
* [Python](https://www.python.org/downloads/),
use the package manager [pip](https://pip.pypa.io/en/stable/) to install these two Python dependencies.

```bash
pip install markdown

pip install jinja2
```

### Building for Wwise

Once the Wwise SDK has been installed you can use the [wp.py](https://www.audiokinetic.com/library/edge/?source=SDK&id=effectplugin_tools.html) file to issue commands relating to the Wwise SDK. This file can be set as [system environment variable](https://superuser.com/questions/284342/what-are-path-and-other-environment-variables-and-how-can-i-set-or-use-them) to save some hassle when issuing commands from the command prompt.

* Open the command prompt in root directory of the current project
* Issue the premake command 
```bash
wp.py premake Authoring
```
* This will create a solution that can now be opened in visual studio, depending on what versions you have selected when installing the Wwise SDK, there may be multiple solutions corresponding to different versions of Visual Studio, **vc160** corresponds to Visual Studio **2019**